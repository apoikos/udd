#!/usr/bin/ruby
# Used by DDPO

$:.unshift('../../rlibs')
require 'udd-db'
require 'yaml'

RELEASE=YAML::load(IO::read('../ubuntu-releases.yaml'))['devel']

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)
DB["select source, version
from ubuntu_sources
where distribution = 'ubuntu'
and release = '#{RELEASE}'
order by source asc"].all.sym2str.each do |row|
  puts "#{row['source']} #{row['version']}"
end
