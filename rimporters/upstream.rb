#!/usr/bin/ruby
# encoding: utf-8

DEB_MIRROR='/srv/mirrors/debian'
# Do at most that many packages at a time
UPSTREAM_LIMIT=100000

module Upstream
  def Upstream.get_watch(source, version, component)
    d = `mktemp -d /tmp/getwatch.#{source}.XXXXXX`.chomp
    raise unless $?.exitstatus == 0
    version = version.gsub(/^[\d]+:/,'')
    path = "#{DEB_MIRROR}/pool/#{component}/#{poolpath(source)}/#{source}_#{version}.dsc"
    system("cd #{d} && dpkg-source --no-check -x #{path} >/dev/null 2>&1")
    if $?.exitstatus != 0
      puts "Failed dpkg-source with #{source} #{path}"
      FileUtils::rm_rf(d)
      return nil
    end
    f = Dir::glob("#{d}/#{source}-*/debian/watch")
    if f.length != 0
      watch = IO::read(f[0])
      watch.encode!('UTF-8', :undef => :replace, :invalid => :replace, :replace => "")
    else
      watch = nil
    end

    f = Dir::glob("#{d}/#{source}-*/debian/upstream/signing-key.pgp")
    if f.length != 0
      #signingkeypgp = PG.escape_bytea(IO::read(f[0]))
      signingkeypgp = nil
    else
      signingkeypgp = nil
    end

    f = Dir::glob("#{d}/#{source}-*/debian/upstream/signing-key.asc")
    if f.length != 0
      signingkeyasc = IO::read(f[0]).force_encoding('utf-8')
    else
      signingkeyasc = nil
    end
    # We are not importing keys at the moment: given we don't download archives, it's useless anyway.
    signingkeypgp = nil

    if signingkeypgp.nil?
      f = Dir::glob("#{d}/#{source}-*/debian/upstream-signing-key.pgp")
      if f.length != 0
        signingkeypgp = IO::read(f[0])
      else
        signingkeypgp = nil
      end
    end
    # We are not importing keys at the moment: given we don't download archives, it's useless anyway.
    signingkeypgp = nil

    FileUtils::rm_rf(d)
    return [watch, signingkeypgp, signingkeyasc ]
  end

  def Upstream.get_status(source, version, watch_file, signing_key_pgp, signing_key_asc)
    # strip debian revision
    version = version.gsub(/-[\da-zA-Z.~+]+$/,'')
    # strip epoch
    version = version.gsub(/^[\d]+:/,'')
    d = `mktemp -d /tmp/watch.#{source}.XXXXXX`.chomp
    FileUtils::mkdir_p("#{d}/debian/upstream")
    File::open("#{d}/debian/watch", 'w') do |fd|
      fd.print watch_file
    end
    ENV['PERL_LWP_SSL_VERIFY_HOSTNAME']='0' 
    if signing_key_pgp
      File::open("#{d}/debian/upstream/signing-key.pgp", 'w') do |fd|
        fd.print signing_key_pgp
      end
    end
    if signing_key_asc
      File::open("#{d}/debian/upstream/signing-key.asc", 'w') do |fd|
        fd.print signing_key_asc
      end
    end
    # make sure that we use /etc/ssl/ca-global even in git; see https://lists.debian.org/debian-qa/2016/05/msg00029.html
    system("dir=/etc/ssl/ca-global && test -d $dir && git config --global --replace-all http.sslCAInfo $dir/ca-certificates.crt")
    cmd = "cd #{d} && HTTPS_CA_DIR=/etc/ssl/ca-global timeout 5m uscan --pasv --dehs --no-download --watchfile debian/watch --package #{source} --upstream-version #{version}"
    #p cmd
    stdout, stderr, status = Open3.capture3(cmd)
    FileUtils::rm_rf(d)
    begin
      d = REXML::Document::new(stdout)
    rescue
      return {
        :debian_uversion => nil,
        :debian_mangled_uversion => nil,
        :upstream_version => nil,
        :upstream_url => nil,
        :status => 'error',
        :errors => "failed to parse XML: #{stdout}",
        :warnings => nil
      }
    end
    if d.root.elements.count == 0
      return {
        :debian_uversion => nil,
        :debian_mangled_uversion => nil,
        :upstream_version => nil,
        :upstream_url => nil,
        :status => 'error',
        :errors => 'uscan returned an empty output',
        :warnings => nil
      }
    elsif d.root.elements['errors']
      return {
        :debian_uversion => nil,
        :debian_mangled_uversion => nil,
        :upstream_version => nil,
        :upstream_url => nil,
        :status => 'error',
        :errors => d.root.elements['errors'].text,
        :warnings => nil
      }
    elsif not (['debian-uversion', 'debian-mangled-uversion', 'upstream-version', 'upstream-url', 'status'] - d.root.elements.to_a.map { |e| e.name }).empty?
      # At least one of the required fields is missing. Count as error.
      return {
        :debian_uversion => (e = d.root.elements['debian-uversion']) ? e.text : nil,
        :debian_mangled_uversion => (e = d.root.elements['debian-mangled-uversion']) ? e.text : nil,
        :upstream_version => (e = d.root.elements['upstream-version']) ? e.text : nil,
        :upstream_url => (e = d.root.elements['upstream-url']) ? e.text : nil,
        :status => 'error',
        :errors => (e = d.root.elements['errors']) ? e.text : nil,
        :warnings => (e = d.root.elements['warnings']) ? e.text : nil
      }
    else
      return {
        :debian_uversion => d.root.elements['debian-uversion'].text,
        :debian_mangled_uversion => d.root.elements['debian-mangled-uversion'].text,
        :upstream_version => d.root.elements['upstream-version'].text,
        :upstream_url => d.root.elements['upstream-url'].text,
        :status => d.root.elements['status'].text,
        :errors => (e = d.root.elements['errors']) ? e.text : nil,
        :warnings => (e = d.root.elements['warnings']) ? e.text : nil
      }
    end
  end

  def Upstream.update_upstream
    db = PG.connect(UDD_USER_PG)

    # Cleanup
    db.exec <<-EOF
DELETE FROM upstream where (source, version, distribution, release, component) IN (SELECT source, version, distribution, release, component from upstream except select source, version, distribution, release, component from sources_uniq where release in ('sid', 'experimental'))
    EOF

    # Get watch file for new packages
    res = db.exec("select source, version, distribution, release, component from sources_uniq
where release in ('sid', 'experimental') except select source, version, distribution, release, component from upstream limit #{UPSTREAM_LIMIT}").to_a
    mutex = Mutex::new
    db.prepare('upstream_insert', "INSERT INTO upstream (source, version, distribution, release, component, watch_file, signing_key_pgp, signing_key_asc, last_check) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)")
    res.peach(20) do |e|
      watch, signingkeypgp, signingkeyasc = get_watch(e['source'], e['version'], e['component'])
      next if e['source'] == 'python-amqp' and e['version'] == '2.4.2-1' # bug in python-amqp #925389
      mutex.synchronize do
        begin
          db.exec_prepared('upstream_insert', [ e['source'], e['version'], e['distribution'], e['release'], e['component'], watch, signingkeypgp, signingkeyasc, nil] )
       rescue
         puts "Exception when inserting watch file information for #{e['source']} #{e['version']}"
         raise
       end
      end
    end

    # Check packages
    res = db.exec(<<-EOF).to_a
select source, version, distribution, release, component, watch_file, signing_key_pgp, signing_key_asc
from upstream
where watch_file is not null and (last_check is null or age(last_check) > '72 hours')
limit #{UPSTREAM_LIMIT}
    EOF
    db.prepare('upstream_update', "UPDATE upstream SET debian_uversion=$1, debian_mangled_uversion=$2, upstream_version=$3, upstream_url=$4, errors=$5, warnings=$6, status=$7,last_check=NOW() where source=$8 and version=$9 and distribution=$10 and release=$11 and component=$12")
    res.peach(20) do |e|
      st = get_status(e['source'], e['version'], e['watch_file'], e['signing_key_pgp'], e['signing_key_asc'])
      mutex.synchronize do
        db.exec_prepared('upstream_update', [ st[:debian_uversion], st[:debian_mangled_uversion], st[:upstream_version], st[:upstream_url], st[:errors], st[:warnings], st[:status], e['source'], e['version'], e['distribution'], e['release'], e['component']] )
      end
    end
  end
end
