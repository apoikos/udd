#!/bin/sh

set -e
set -x
sudo service postgresql stop

echo "Starting SSH tunnel (CTRL+C to shutdown)"
# for alioth
#ssh -NL 5452:udd.debian.org:5452 aliothlogin@alioth.debian.org

# if you have access to ullman:
ssh -NL 5452:localhost:5452 lucas@ullmann.debian.org
